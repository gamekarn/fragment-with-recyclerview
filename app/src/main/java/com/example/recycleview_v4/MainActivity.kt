package com.example.recycleview_v4

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recycleview_v4.adapter.itemCheckedAdapter
import com.example.recycleview_v4.listener.OnitemClickedListener
import com.example.recycleview_v4.model.Commentmodel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.*
import java.io.IOException

class MainActivity : AppCompatActivity(), OnitemClickedListener {

    private lateinit var layoutmanager: LinearLayoutManager
    private lateinit var adapter: itemCheckedAdapter
    var dataList: ArrayList<Commentmodel> = arrayListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getComments()
        setLayoutManager()
        setupRecyclerView()
    }

    private fun getComments() {
        val endpoint = "https://jsonplaceholder.typicode.com/posts/1/comments"

        val client = OkHttpClient()

        val request = Request.Builder()
            .url(endpoint)
            .build()

//        val response = client.newCall(request).execute()

        val call = client.newCall(request).enqueue(object : Callback {

            override fun onResponse(call: Call, response: Response) {
                if (response.isSuccessful) {
                    response.body?.let {
                        val data = it.string()
//                        Log.d("MainActivity", "Comments $data")
                        mapDataToJson(data)
                        runOnUiThread {
                            setAdapter()
                        }
                    }
                }
            }

            override fun onFailure(call: Call, e: IOException) {

            }

        })
    }

    private fun mapDataToJson(data: String) {
        val gson = Gson()
        val collectionType = object : TypeToken<List<Commentmodel>>() {}.type
        dataList = gson.fromJson<String>(data, collectionType) as ArrayList<Commentmodel>
        //        Log.d("MainActivity", "CommentListModel= $dataList")
        runOnUiThread {
            setAdapter()
        }
    }


    private fun setLayoutManager() {
        layoutmanager = LinearLayoutManager(this)
    }

    private fun setupRecyclerView() {
//        container.layoutManager = layoutmanager
//        container.setHasFixedSize(true)
    }

    private fun setAdapter() {
        adapter = itemCheckedAdapter()
        adapter.listener = this@MainActivity
        adapter.dataList = dataList
//        container.adapter = adapter
    }

    override fun onCliked(position: Int) {
        println("this is $position")
    }

    override fun OnCheckedChangeListener(i: Int) {
        dataList[i].isChecked = !dataList[i].isChecked
    }


}
