package com.example.recycleview_v4.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.recycleview_v4.listener.OnitemClickedListener
import com.example.recycleview_v4.model.Commentmodel
import kotlinx.android.synthetic.main.item_recycleview_adapter.view.*

class itemCheckedViewHolder(var view: View) : RecyclerView.ViewHolder(view) {

    fun bindViewHolder(position: Int, listener: OnitemClickedListener, dataList: ArrayList<Commentmodel>) {
        view.rootView.setOnClickListener {
            listener.onCliked(position)
        }
        view.checkBox.setOnClickListener {
            listener.OnCheckedChangeListener(position)
        }
        view.checkBox.isChecked = dataList[position].isChecked

        view.title.text = dataList[position].name

        view.description.text = dataList[position].body
    }
}