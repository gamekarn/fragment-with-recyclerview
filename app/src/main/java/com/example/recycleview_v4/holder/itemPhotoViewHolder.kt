package com.example.recycleview_v4.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.recycleview_v4.listener.OnitemClickedListener
import com.example.recycleview_v4.model.photoModel
import kotlinx.android.synthetic.main.item_recycleview_adapter.view.*

class itemPhotoViewHolder(var view: View) : RecyclerView.ViewHolder(view) {

    fun bindViewHolder(position: Int, listener: OnitemClickedListener, dataList: ArrayList<photoModel>) {
        val model = dataList[position]
        view.rootView.setOnClickListener {
            listener.onCliked(position)
        }
        view.checkBox.setOnClickListener {
            listener.OnCheckedChangeListener(position)
        }

        view.title.text = model.title

        view.description.text = model.url

        Glide.with(view.context).load(model.thumbnailUrl).into(view.imageView)
    }
}