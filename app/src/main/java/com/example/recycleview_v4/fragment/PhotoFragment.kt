package com.example.recycleview_v4.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recycleview_v4.R
import com.example.recycleview_v4.adapter.itemPhotoAdapter
import com.example.recycleview_v4.listener.OnitemClickedListener
import com.example.recycleview_v4.model.photoModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_photo.*
import okhttp3.*
import java.io.IOException


class PhotoFragment : Fragment(), OnitemClickedListener {

    private lateinit var dataList: ArrayList<photoModel>
    private lateinit var layoutmanager: LinearLayoutManager
    private lateinit var adapter: itemPhotoAdapter

    companion object {
        fun newInstance(): PhotoFragment {
            return PhotoFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            dataList = arrayListOf()
        } else {
            dataList = savedInstanceState.getParcelableArrayList("Photolist")!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_photo, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setLayoutManager()
        setupRecyclerView()
        setRefreshDataListener()
    }

    private fun setRefreshDataListener() {
        refleshLayout.setOnRefreshListener {
            dataList.clear()
            adapter.notifyDataSetChanged()
            getPhoto()
        }
    }

    private fun showLoading() {
        refleshLayout.isRefreshing = true
    }

    private fun hideLoading() {
        refleshLayout.isRefreshing = false
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (dataList.isEmpty()) {
            showLoading()
            getPhoto()
        } else {
            setAdapter()
        }
    }

    private fun setLayoutManager() {
        layoutmanager = LinearLayoutManager(activity)
    }

    private fun setupRecyclerView() {
        recyclerView.layoutManager = layoutmanager
        recyclerView.setHasFixedSize(true)
    }

    private fun getPhoto() {
        val endpoint = "https://jsonplaceholder.typicode.com/photos"

        val client = OkHttpClient()

        val request = Request.Builder()
            .url(endpoint)
            .build()

        val call = client.newCall(request).enqueue(object : Callback {

            override fun onResponse(call: Call, response: Response) {
                if (response.isSuccessful) {
                    response.body?.let {
                        val data = it.string()
                        mapDataToJson(data)
                        activity?.runOnUiThread {
                            hideLoading()
                            setAdapter()
                        }
                    }
                }
            }

            override fun onFailure(call: Call, e: IOException) {

            }
        })
    }

    private fun mapDataToJson(data: String) {
        val gson = Gson()
        val collectionType = object : TypeToken<List<photoModel>>() {}.type
        dataList = gson.fromJson<String>(data, collectionType) as ArrayList<photoModel>
        activity?.runOnUiThread {
            setAdapter()
        }
    }

    private fun setAdapter() {
        adapter = itemPhotoAdapter()
        adapter.listener = this
        adapter.dataList = dataList
        recyclerView.adapter = adapter
    }

    override fun onCliked(position: Int) {
        println("this is $position")

    }

    override fun OnCheckedChangeListener(i: Int) {

    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList("Photolist", dataList)
    }


}