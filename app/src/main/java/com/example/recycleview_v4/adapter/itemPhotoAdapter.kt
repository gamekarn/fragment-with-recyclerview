package com.example.recycleview_v4.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.recycleview_v4.R
import com.example.recycleview_v4.holder.itemPhotoViewHolder
import com.example.recycleview_v4.listener.OnitemClickedListener
import com.example.recycleview_v4.model.photoModel

class itemPhotoAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var listener: OnitemClickedListener
    var dataList: ArrayList<photoModel> = arrayListOf()

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(
                R.layout.photo_recycleview_adapter,
                parent, false
            )
        return itemPhotoViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is itemPhotoViewHolder -> {
                holder.bindViewHolder(position, listener, dataList)
            }
        }
    }
}