package com.example.recycleview_v4.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.recycleview_v4.R
import com.example.recycleview_v4.holder.itemCheckedViewHolder
import com.example.recycleview_v4.listener.OnitemClickedListener
import com.example.recycleview_v4.model.Commentmodel

class itemCheckedAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var listener: OnitemClickedListener
    var dataList: ArrayList<Commentmodel> = arrayListOf()

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(
                R.layout.item_recycleview_adapter,
                parent, false
            )
        return itemCheckedViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is itemCheckedViewHolder -> {
                holder.bindViewHolder(position, listener, dataList)
            }
        }
    }
}