package com.example.recycleview_v4.model

import com.google.gson.annotations.SerializedName

data class Commentmodel(
    @SerializedName("postId") var postId: String,
    @SerializedName("id") var id: String,
    @SerializedName("name") var name: String,
    @SerializedName("email") var email: String,
    @SerializedName("body") var body: String,
    var isChecked: Boolean
) {

}