package com.example.recycleview_v4.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class photoModel(
    @SerializedName("albumID") var albumId: Int,
    @SerializedName("id") var id: Int,
    @SerializedName("title") var title: String?,
    @SerializedName("url") var url: String?,
    @SerializedName("thumbnailUrl") var thumbnailUrl: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(albumId)
        parcel.writeInt(id)
        parcel.writeString(title)
        parcel.writeString(url)
        parcel.writeString(thumbnailUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<photoModel> {
        override fun createFromParcel(parcel: Parcel): photoModel {
            return photoModel(parcel)
        }

        override fun newArray(size: Int): Array<photoModel?> {
            return arrayOfNulls(size)
        }
    }
}