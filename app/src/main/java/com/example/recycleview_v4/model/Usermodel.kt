package com.example.recycleview_v4.model

import com.google.gson.annotations.SerializedName

data class Usermodel(
    @SerializedName("name") var name: String,
    @SerializedName("age") var age: Int,
    @SerializedName("cars") var cars: List<CarModel>
) {

}

data class CarModel(
    @SerializedName("name") var name: String,
    @SerializedName("models") var models: List<String>
) {
}
