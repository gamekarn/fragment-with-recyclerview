package com.example.recycleview_v4

import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recycleview_v4.adapter.itemPhotoAdapter
import com.example.recycleview_v4.fragment.PhotoFragment
import com.example.recycleview_v4.model.photoModel

class PhotoActivity : AppCompatActivity() {

    private lateinit var layoutmanager: LinearLayoutManager
    private lateinit var adapter: itemPhotoAdapter
    var dataList: ArrayList<photoModel> = arrayListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            replaceFragment()
        }
    }


    private fun replaceFragment() {
        var fragment = PhotoFragment.newInstance()
        var transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.commit()
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
//        outState.putParcelableArrayList("Photolist", dataList)
    }


}
